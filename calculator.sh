#!/usr/bin/env bash

case ${1} in
    "add") #dodawanie
        ((result=${2}+${3}))
        echo "${result}"
        ;;
    "subtract") #odejmowanie
        ((result=${2}-${3}))
        echo "${result}"
        ;;
    "multiply") #mnozenie
        ((result=${2}*${3}))
        echo "${result}"
        ;;
    "divide") #dzielenie
        if((${3}!=0));then #sprawdzam czy trzecia wartosc to 0
            ((result=${2}/${3}))
            echo "${result}"
        else
            echo "Error" #jesli trzecia wartosc to 0 to zwracam blad
        fi
        ;;
    *) #gdy uzytkownik poda zla operacje
        echo "Error"
        ;;
esac